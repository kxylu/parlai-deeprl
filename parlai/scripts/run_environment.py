#!/usr/bin/env python3

# Copyright (c) Facebook, Inc. and its affiliates.
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

"""
Running the training simulator

"""  # noqa: E501

# TODO List:
# * More logging (e.g. to files), make things prettier.

from parlai.core.params import ParlaiParser
from parlai.agents.repeat_label.repeat_label import RepeatLabelAgent
from parlai.core.worlds import create_task
from parlai.utils.strings import colorize
from parlai.core.script import ParlaiScript, register_script
import parlai.utils.logging as logging

import random


def setup_args(parser=None) -> ParlaiParser:
    """
    Build the ParlAI parser, adding command line args if necessary.

    :param ParlaiParser parser:
        Preexisting parser to append options to. Will be created if needed.

    :returns:
        the ParlaiParser with CLI options added.
    """
    if parser is None:
        parser = ParlaiParser(True, True, 'Train a model')
    parser.add_argument('-n', '-ne', '--num-examples', type=int, default=10)
    return parser
def run(opt):
        # if python is called from a non-interactive shell, like a bash script,
        # it will by-default ignore SIGINTs, and KeyboardInterrupt exceptions are
        # not produced. This line brings them back
        opt.log()
        agent = RepeatLabelAgent(opt)
        world = create_task(opt, agent)
        # set up timerself.parleys = 0
        while True:
            world.parley()

            world.display()
            if world.epoch_done():
                logging.info('epoch done')
                break



@register_script('run_environment', aliases=['run_env'])
class RunEnvironment(ParlaiScript):
    @classmethod
    def setup_args(cls):
        return setup_args()

    def run(self):
        return run(self.opt)


if __name__ == '__main__':
    RunEnvironment.main()
