from parlai.core.torch_agent import TorchAgent

class ActorCriticAgent(TorchAgent):
    def __init__(self, opt, shared):
        super(ActorCriticAgent).__init__(opt, shared)