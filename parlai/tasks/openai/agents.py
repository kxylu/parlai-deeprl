from parlai.core.teachers import Teacher
import gym
import gym.spaces
import random


class PongTeacher(Teacher):
    def __init__(self, opt, shared=None):
        self.id = "pong"
        self.env_name = "PongNoFrameskip-v4"
        self.num_episodes = opt.get("num_episodes", 1)
        self.env = gym.make(self.env_name)
        self.reset()

    def observe(self, observation):
        if "action" not in observation:
            self.action = [[random.randrange(4)]]
        else:
            self.action = observation["action"]

    def act(self):
        text = "Text Display not available in atari game."

        if self.epochDone:
            obs = self.env.reset()
            text = f"Observation from environment {obs}"
            self.epochDone = False
            return {"text":text, "obs": obs, "reward": 0, "episode_done": False, "info":""}
        obs, reward, done, info = self.env.step(self.action)
        if done:
            self.epochDone = True
        return {"text":text, "obs": obs, "reward": reward, "episode_done":done, "info":info}

    def reset(self):
        # super().reset()
        self.epochDone = True

    


class DefaultTeacher(PongTeacher):
    pass
